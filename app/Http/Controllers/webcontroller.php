<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\signupmodel;
use App\addhotelmodel;
use App\userreservemodel;
use DB;
use Validator;
use Auth;

class webcontroller extends Controller
{ 
    //Initial dashboard and page.
    public function getInitialpage(){
    	return view('initialpage');
    }
     public function getAdmindashboard(){
      return view('admindashboard');
    }

    //login function.
     public function getLogin(){
    	return view('login');
    }

public function postLogin(Request $request){
$obj=new signupmodel();
 $username=$request->get('username');
 $password=$request->get('password');

if(Auth::attempt(['username'=>$username,'password'=>$password])){
if($que=DB::select("select * from signupmodels where username='$username'")){
  foreach ($que as $row) {
    # code...
    $role=$row->role;
  }
  if($role==0){
    return redirect('hotel/admindashboard');
  }
  else{
    return redirect('user/userdashboard');
  }
}
else{
  return redirect('hotel/login');
}
}
}
// $query=DB::select("select username,password from signupmodels where username='$username' and password='$password'");
// if($query){
 
// //echo "Success";
//   return redirect ('user/userdashboard');
// }
// else{
// //echo "sorry";
//     return redirect ('hotel/login');
// }

    
    
    //views 
    public function getHotelview(){
    $obj=addhotelmodel::all();
    return view('hotelview',array('result'=>$obj));
    }
//recently
 public function getAdminhotelview(){
    $obj=addhotelmodel::all();
    return view('adminhotelview',array('result'=>$obj));
    }

    //edit function.
    public function getHoteledit(){
    $obj=addhotelmodel::all();
    return view('hoteledit',array('result'=>$obj));
    }
     public function getUpdatehotel($id){
      $edit=addhotelmodel::find($id);
    return view('updatehotel',array('result'=>$edit));
    }
    public function postUpdatehotel($id){     
    $edit=addhotelmodel::find($id);
    $edit->roomno=Input::get('roomno');
    $edit->roomtype=Input::get('roomtype');
    $edit->image=Input::get('image');
    $edit->price=Input::get('price');
    $edit->save();
    return redirect('hotel/hotelview');
}
        public function getUserview(){
$obj=signupmodel::all();
return view('userview',array('result'=>$obj));
    }

     //Delete function.
      public function getHoteldelete(){
   $obj=addhotelmodel::all();
return view('hoteldelete',array('result'=>$obj));
    }
    public function getDelete($id){
      $delete=addhotelmodel::find($id);
$delete->delete();
return redirect('hotel/hoteldelete');
    }

//Adding Hotel.
     public function getAdminaddhotel(){
    	return view('adminaddhotel');
    }
    public function postAdminaddhotel(Request $request){
$obj=new addhotelmodel();

$roomno=$request->get('roomno');
$roomtype=$request->get('roomtype');
$file=$request->file('image');
$destination_path='lib/hotelimage/';
$filename=$file->getClientOriginalName();
$file->move($destination_path,$filename);
$image=$destination_path.$filename;
$price=$request->get('price');

$obj->roomno=$roomno;
$obj->roomtype=$roomtype;
$obj->image=$image;
$obj->price=$price;

  $result=$obj->save();
  if ($result){
return redirect('hotel/hotelview');
//echo "Success";
  }else{
echo "Sorry";
  }
    }

//Signup models.
     public function getSignup(){
        return view('signup');
    }
    public function postSignup(Request $request){
        $obj=new signupmodel();

$validation=Validator::make($request->all(),[
'username'=>'required|unique:signupmodels,username',
'password'=>'required|alpha|min:5|max:10',
'firstame'=>'required|alpha',
'lastame'=>'required|alpha',
'age'=>'required',
'hometown'=>'required|alpha',
'gender'=>'required|alpha',
'username'=>'required',
'password'=>'required',
'confirmpassword'=>'required|alpha',
  ]);
if($validation->fails()){
  return view('signup')->with('errors',$validation->errors());
}

$firstame=$request->get('firstame');
$lastame=$request->get('lastame');
$age=$request->get('age');
$hometown=$request->get('hometown');
$gender=$request->get('gender');
$username=$request->get('username');
$password=Encrypt($request->get('password'));
$confirmpassword=Encrypt($request->get('confirmpassword'));
$role=$request->get('role');

$obj->firstame=$firstame;
$obj->lastame=$lastame;
$obj->age=$age;
$obj->hometown=$hometown;
$obj->gender=$gender;
$obj->username=$username;
$obj->password=$password;
$obj->confirmpassword=$confirmpassword;
$obj->role=$role;

$result=$obj->save();
if($result){
    return redirect('hotel/login');
//echo "Successfull";
}
else {
//return redirect('form/index');
echo "Sorry";
}  
 }  

//reserve view.
        public function getAdminreserveview(){
$obj=userreservemodel::all();
return view('adminreserveview',array('result'=>$obj));
    }

}
 