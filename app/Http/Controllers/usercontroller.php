<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\addhotelmodel;
use App\userreservemodel;
use Validator;

class usercontroller extends Controller
{
    //
    public function getUserdashboard(){
    	return view('userdashboard');
    }

    public function getUserhotelview(){
    $obj=addhotelmodel::all();
    $obj1   = userreservemodel::all();
    return view('userhotelview',array('result'=>$obj,'result1'=>$obj1));
    }

     public function getUserreserve($id){
    	return view('userreserve')->with('id',$id);
    }
    //main done here recently.
public function postUserreserve(Request $request,$id){
$obj=new userreservemodel();        
$obj1  = addhotelmodel::find($id);

$obj1->booked = 'yes';
$obj1->save();
$obj->room_id = $id;

$checkin=$request->get('checkin');
$checkout=$request->get('checkout');
$nights=$request->get('nights');
$stayingmembers=$request->get('stayingmembers');
$creditnumbers=$request->get('creditnumbers');
$cvc=$request->get('cvc');

$obj->checkin=$checkin;
$obj->checkout=$checkout;
$obj->nights=$nights;
$obj->stayingmembers=$stayingmembers;
$obj->creditnumbers=$creditnumbers;
$obj->cvc=$cvc;

$result=$obj->save();
if($result){
    return redirect('user/userhotelview');
//echo "Successfull";
}
else {
//return redirect('form/index');
echo "Sorry";
} 
}
}
