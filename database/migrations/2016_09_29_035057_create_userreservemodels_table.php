<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserreservemodelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userreservemodels', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('room_id');
            $table->date('checkin');
            $table->date('checkout');
            $table->string('nights');
            $table->string('stayingmembers');
            $table->string('creditnumbers');
            $table->string('cvc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('userreservemodels');
    }
}
