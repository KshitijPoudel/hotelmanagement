<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddhotelmodelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addhotelmodels', function (Blueprint $table) {
            $table->increments('id');
              $table->string('roomno');
                $table->string('roomtype');
                  $table->string('image');
                    $table->string('price');
            $table->string('booked');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('addhotelmodels');
    }
}
