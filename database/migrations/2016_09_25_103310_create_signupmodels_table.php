<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSignupmodelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signupmodels', function (Blueprint $table) {
            $table->increments('id');
              $table->string('firstame');
             $table->string('lastame');
             $table->string('age');
             $table->string('hometown');
             $table->string('gender');
             $table->string('username');
             $table->string('password');
             $table->string('confirmpassword');
              $table->string('role');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('signupmodels');
    }
}
