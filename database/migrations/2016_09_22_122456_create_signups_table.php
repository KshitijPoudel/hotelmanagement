<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSignupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signups', function (Blueprint $table) {
            $table->increments('id');
             $table->string('First_Name');
             $table->string('Last_Name');
             $table->string('Age');
             $table->string('Hometown');
             $table->string('Gender');
             $table->string('Username');
             $table->string('Password');
             $table->string('Confirm_Password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('signups');
    }
}
