<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('signupmodels')->insert([
       	'firstame'=>'Kshitij',
        'lastame'=>'Poudel',
         'age'=>'33',
         'hometown'=>'Balaju',
          'gender'=>'Male',
           'username'=>'admin',
           'password'=>Hash::make('admin'),
           'confirmpassword'=>Hash::make('admin'),
           'role'=>'0'
           ]);
    }
}
