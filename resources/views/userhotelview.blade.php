<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Hotel Reservations</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="{{url('admindashboard/css/style.css')}}" type="text/css" media="all" />

<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico" />
<!--[if IE 6]><link rel="stylesheet" href="{{url('admindashboard/css/ie.css')}}" type="text/css" media="all" /><![endif]-->
</head>
<body>
<!-- START PAGE SOURCE -->
<div id="header">
  <div class="shell">
    <h1 id="logo"><a href="#">WCSST 37</a></h1>
    <div id="navigation">
      <ul>
      <li><a href="{{url('user/userdashboard')}}">HOME</a></li>
        <li><a href="{{url('user/userhotelview')}}"  class="active">VIEW ROOM'S</a></li>
        <li><a href="#">ABOUT US</a></li>
        <li><a href="#">CONTACT US</a></li>
         <li><a href="#">OPTIONS</a></li>
       
        
      </ul>
    </div>
  </div>
</div>
<div id="forms">
<form action="" method="POST" enctype="multipart/form-data">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
  <input type="hidden" name="_token" value="{{csrf_token()}}">
   <h2 style="color:white;font-size:25px;font-weight:bold;text-align:center;line-height:70px;">
<table border=2px>
  <tr>
    <th>id</th>
      <th>Room_NO</th>
      <th>Room_Type</th>
      <th>Image</th>
       <th>Price</th>
      </tr>

      @foreach($result as $row)
      <tr>
        <th>{{$row['id']}}</th>
        <th>{{$row['roomno']}}</th>
        <th>{{$row['roomtype']}}</th>
        <th><img src="{{url($row['image'])}}" width="350px" height="150px"></th>
        <th>{{$row['price']}}</th>
        
       @if($row->booked=='yes')
       <th>Reserved</th>
          @else<th><a href="{{url('user/userreserve')}}/{{$row['id']}}">RESERVE</a></th>
         @endif
      </tr>
      @endforeach
    </tr>
  </table>
<h2>
</form>
</div>
</body>
</html>