<!DOCTYPE html>
<html>

<head>

  <meta charset="UTF-8">

  <title>CodePen - Login</title>

    <link rel="stylesheet" href="{{url('SignUp/signup/css/style.css')}}" media="screen" type="text/css" />

</head>

<body>

  <html lang="en-US">
  <head>

    <meta charset="utf-8">

    <title>Login</title>

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,700">

    <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->

  </head>

  <body>
    <h1 style="color:white;line-height:150px;font-weight:bold;font-size:50px;
text-align:center;">
Login Here!!!!!!</h1>
    <div class="container">

      <div id="login">

        <form action="{{url('hotel/login')}}" method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
          <fieldset class="clearfix">

            <p><span class="fontawesome-user"></span><input type="text" value="Username" name="username"/></p> <!-- JS because of IE support; better: placeholder="Username" -->
            <p><span class="fontawesome-user"></span><input type="password" value="Password" name="password"/></p> <!-- JS because of IE support; better: placeholder="Password" -->
            <p><input type="submit" value="LOGIN" name="login"></p>

          </fieldset>

        </form>

        <p>Not a member? <a href="{{url('hotel/signup')}}">Sign up now</a><span class="fontawesome-arrow-right"></span></p>

      </div> <!-- end login -->

    </div>

  </body>
</html>

</body>

</html>