<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Hotel Reservations</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="{{url('admindashboard/css/style.css')}}" type="text/css" media="all" />

<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico" />
<!--[if IE 6]><link rel="stylesheet" href="{{url('admindashboard/css/ie.css')}}" type="text/css" media="all" /><![endif]-->
</head>
<body>
<!-- START PAGE SOURCE -->
<div id="header">
  <div class="shell">
    <h1 id="logo"><a href="#">WCSST 37</a></h1>
    <div id="navigation">
      <ul>
        <li><a href="{{url('hotel/admindashboard')}}">HOME</a></li>
        <li><a href="{{url('hotel/adminaddhotel')}}">ADD ROOM'S</a></li>
        <li><a href="{{url('hotel/hotelview')}}">VIEW ROOMS</a></li>
        <li><a href="{{url('hotel/hoteledit')}}"   class="active">EDIT</a></li>
         <li><a href="{{url('hotel/hoteldelete')}}">DELETE</a></li>
          <li><a href="{{url('hotel/userview')}}">USER DETAIL</a></li>
       
        
      </ul>
    </div>
  </div>
</div>
<div id="forms">
<form action="" method="POST" enctype="multipart/form-data">
  <input type="hidden" name="_token" value="{{csrf_token()}}">
   <h2 style="color:white;font-size:30px;font-weight:bold;text-align:center;line-height:70px;">
Room_NO<input type="text" name="roomno" value="{{$result->roomno}}"></br>
Room_Type<select name="roomtype" value="{{$result->roomtype}}">
<option>Single Bed</option>
<option>Double Bed</option>
<option>Attached Double Bed</option>
<option>Triple Bed</option>
<option>Attached Triple Bed</option>
</select></br>
Image<input type="file" name="image" value="{{$result->image}}"></br>
Price<input type="text" name="price" value="{{$result->price}}"></br>
<input type="Submit" value="ADD">
<h2>
</form>
</div>
</body>
</html>