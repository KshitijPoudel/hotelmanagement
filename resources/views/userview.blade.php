  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <title>Online Hotel Reservations</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" href="{{url('admindashboard/css/style.css')}}" type="text/css" media="all" />

  <link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico" />
  <!--[if IE 6]><link rel="stylesheet" href="{{url('admindashboard/css/ie.css')}}" type="text/css" media="all" /><![endif]-->
  </head>
  <body>
  <!-- START PAGE SOURCE -->
  <div id="header">
    <div class="shell">
      <h1 id="logo"><a href="#">WCSST 37</a></h1>
      <div id="navigation">
        <ul>
          <li><a href="{{url('hotel/admindashboard')}}">HOME</a></li>
          <li><a href="{{url('hotel/adminaddhotel')}}">ADD ROOM'S</a></li>
          <li><a href="{{url('hotel/hotelview')}}">VIEW ROOMS</a></li>
          <li><a href="{{url('hotel/hoteledit')}}">EDIT</a></li>
           <li><a href="{{url('hotel/hoteldelete')}}">DELETE</a></li>
            <li><a href="{{url('hotel/userview')}}"  class="active">USER DETAIL</a></li>
               <li><a href="{{url('hotel/adminreserveview')}}">RESERVED DETAIL</a></li>          
        </ul>
      </div>
    </div>
  </div>
  <div id="forms">
  <form action="" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
     <h2 style="color:white;font-size:20px;font-weight:bold;text-align:center;line-height:50px;">
  <table border=2px>
    <tr>
      <th>id</th>
      <th>First_Name</th>
      <th>Last_Name</th>
      <th>Age</th>
      <th>Hometown</th>
      <th>Gender</th>
      <th>UserName</th>   
        </tr>
        @foreach($result as $row)
        <tr>
        <th>{{$row['id']}}</th>
        <th>{{$row['firstame']}}</th>
        <th>{{$row['lastame']}}</th>
        <th>{{$row['age']}}</th>
        <th>{{$row['hometown']}}</th>
        <th>{{$row['gender']}}</th>
        <th>{{$row['username']}}</th>     
           </tr>
        @endforeach
      </tr>
    </table>
  <h2>
  </form>
  </div>
  </body>
  </html>