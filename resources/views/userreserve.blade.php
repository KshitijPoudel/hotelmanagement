<!doctype html>
<html lang="en-US">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html">
  <title>Reserve Here</title>
  <meta name="author" content="Jake Rocheleau">
  <link rel="shortcut icon" href="http://static.tmimgcdn.com/img/favicon.ico">
  <link rel="icon" href="http://static.tmimgcdn.com/img/favicon.ico">
  <link rel="stylesheet" type="text/css" media="all" href="{{url('userreserve/css/styles.css')}}">
  <link rel="stylesheet" type="text/css" media="all" href="{{url('userreserve/css/switchery.min.css')}}">

   <link rel="stylesheet" type="text/css" media="all" href="{{url('date/styles/glDatePicker.darkneon.css')}}">
    <link rel="stylesheet" type="text/css" media="all" href="{{url('date/styles/glDatePicker.default.css')}}">
     <link rel="stylesheet" type="text/css" media="all" href="{{url('date/styles/glDatePicker.flatwhite.css')}}">

  <script type="text/javascript" src="{{url('userreserve/js/switchery.min.js')}}"></script>

  <script type="text/javascript" src="{{url('date/glDatePicker.js')}}"></script>
   <script type="text/javascript" src="{{url('date/glDatePicker.min.js')}}"></script>
</head>

<body>
  <div id="wrapper">
  
  <h1>Register for Reservations here.....</h1>
  
 <form action="{{url('user/userreserve')}}/{{$id}}" method="POST">
  <input type="hidden" name="_token" value="{{csrf_token()}}">
  <div class="col-2">
    <label>
    
  Check In<input type="date" name="checkin">
    </label>
  </div>
  <div class="col-2">
    <label>
    Check Out<input type="date" name="checkout">
    </label>
  </div>
  
  <div class="col-3">
    <label>
    Total Night Stay<input type="text" name="nights">	
    </label>
  </div>
  <div class="col-3">
    <label>
      
      Totall Member's<input type="text" name="stayingmembers">	
    </label>
  </div>
  <div class="col-3">
    <label>
      
      Credit Number<input type="text" name="creditnumbers">	
    </label>
  </div>
  <div class="col-3">
    <label>
      
      C V V 2/C V C 2<input type="text" name="cvc">	
    </label>
  </div>
  


  
  <div class="col-submit">
    <input type="submit" value="PROCEED" name="nights">
  </div>
  
  </form>
  </div>
<script type="text/javascript">
var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

elems.forEach(function(html) {
  var switchery = new Switchery(html);
});
</script>
</body>
</html>


